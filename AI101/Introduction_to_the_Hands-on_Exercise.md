<img src="images/IDSNlogo.png" width = "300">

# Introduction to the Hands-on Exercise

## About this exercise:

In this lesson, we'd like to take you on a bit of a side journey. It's a fun exercise and we hope you enjoy it as much as we enjoyed putting this short exercise together for you.

## Image Classification with IBM Watson Visual Recognition:

What better way to understand the applications of AI than to try it out yourself? You'll be uploading images and seeing how IBM Watson identifies the various objects in your images.

<img src="images/lab1.png"  width = 600>

To learn how to classify your own images, continue on to the next reading!

