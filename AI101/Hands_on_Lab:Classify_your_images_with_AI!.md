 
<img src="images/IDSNlogo.png" width = "300">


# Hands on Lab: Classify your images with AI!

## Lab overview:

#### Scenario

IBM Watson Visual Recognition (VR) is a service that uses deep learning algorithms to identify objects and other content in an image. In this hands-on lab, you will use Watson VR to upload and classify images. 

**Note:** To complete this exercise, you will create an IBM Cloud account and provision an instance of the Watson Visual Recognition service. A credit card is NOT required to sign up for an IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson VR service. Objectives

After completing this lab, you will be able to:

1. Access IBM Cloud
2. Add resources to your IBM Cloud account
3. Add services to your IBM Cloud account
4. Create a project in Watson Studio
5. Analyze images using Watson VR

## Exercise 1: Create an IBM Cloud Account

#### Scenario

To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. 

If you already have an IBM Cloud account, you can skip Tasks 1 and 2 and proceed with *Task 3: Login to you IBM Cloud account.*

#### Task 1: Sign up for IBM Cloud

1.  Go to: <a href=https://cloud.ibm.com/registration >Create a free account on IBM Cloud</a>

2. 2. In the **Email** box, enter your email address and then click the arrow.


<img src="images/download1.png" width = "600">

3. When your email address is accepted, enter your **First Name, Last Name, Country or Region**, and create a **Password**.

**Note:** To get enhanced benefits, please sign up with your company email address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the option to be notified by email.

4. Click **Create Account** to create your IBM Cloud account.

#### Task 2: Confirm your email address

1. An email is sent to the address that you signed up with.

<img src="images/download2.png" width = "400">

2. Check your email, and in the email that was sent to you, click **Confirm Account**.

<img src="images/download3.png" width = "600">

3. You will receive notification that your account is confirmed.

<img src="images/download4.png" width = "600">

Click **Log In**, and you will be directed to the IBM Cloud Login Page.

#### Task 3: Login to your IBM Cloud account

1. On the <a href=https://cloud.ibm.com/login >Log in to IBM Cloud page</a> , in the **ID** box, enter your email address and then click **Continue**.

<img src="images/download5.png" width = "600">

2. In the Password box, enter your **password**, and then click **Log in**.

<img src="images/6.png" width = "600">

## Exercise 2: Create a Watson Studio Resource

#### Scenario

To manage all your projects, you will use IBM Watson Studio. In this exercise, you will add Watson Studio as a Resource.

#### Task 1: Add Watson Studio as a resource

1. On the Dashboard, click **Create Resource**.

<img src="images/7.png" width = "600">

2. In the Catalog, click **AI (16)**.

<img src="images/8.png" width = "600">

Note that the **Lite** Pricing plan is selected.

3. In the list of **Services**, click **Watson Studio**.

<img src="images/9.png" width = "600">

4. On the Watson Studio page, select the region closest to you, verify that the **Lite** plan is selected, and then click **Create**.

<img src="images/10.png" width = "600">


5. When the Watson Studio resource is successfully created, you will see the Watson Studio page. Click **Get Started**.

<img src="images/11.png" width = "600">


6. You will see this message when Watson Studio is successfully set up for you.

<img src="images/12.png" width = "400">


# Exercise 3: Create a project

#### Scenario

To manage all the resources and services that you are working with, you should create a Watson Studio Project. You will begin by creating an empty project, and then adding the resources and services that you need. 

#### Task 1: Create an empty project

1. On the Watson Studio Welcome page, click **Create a project**.

<img src="images/13.png" width = "600">


2. On the Create a project page, click **Create an empty project**.

<img src="images/14.png" width = "600">


3. On the New project page, enter a **Name** and **Description** for your project.

<img src="images/15.png" width = "600">


4. You must define storage for your project before you can create it. Under **Select storage service**, click **Add**.

<img src="images/16.png" width = "600">


5. On the Cloud Object Storage page, verify that **Lite** is selected, and then click **Create**.

<img src="images/17.png" width = "600">


6. In the Confirm Creation box, click **Confirm**.

<img src="images/18.png" width = "400">


7. On the New project page, under **Define storage**, click **Refresh**, and then click **Create**.

# Exercise 4: Add a Watson VR Service instance

#### Scenario

This project will focus on analyzing images, so you need to add the Watson Visual Recognition Service. You will also need some images to analyze, so follow the setup steps below to ensure you are prepared.

#### Setup

Before you begin this exercise, you must complete the following steps:

1. Collect a set of at least 20 images. You can use your own images, or download them from the internet.

2. Store the images in an easy to find location.

#### Task 1: Add the Visual Recognition Service

1. To add services to the project, click **Add to project**.

<img src="images/19.png" width = "600">


2. In the Choose asset type box, click **Visual Recognition**.

<img src="images/20.png" width = "600">


3. In the Associate a service box, click **here**.

<img src="images/21.png" width = "600">


4. On the Visual Recognition page, verify that **Lite** is selected, and then click **Create**.

<img src="images/22.png" width = "600">


5. In the Confirm Creation box, click **Confirm**.

<img src="images/23.png" width = "600">


#### Task 2: Analyze images with Watson VR

Now you can see all the built-in image classification models that IBM Watson provides! Let's try the General model.

1. To analyze your images, on the Models page, under **Pre Built Models**, in the **General** box, click **Test**.

<img src="images/24.png" width = "600">


2. On the General page, click the **Test** tab.

<img src="images/25.png" width = "600">


3. To upload images, on the Test tab, click **Browse**.

<img src="images/26.png" width = "600">


4. Select the images you want to upload and then click **Open**.

<img src="images/28.png" width = "600">


5. Once you have uploaded your images, Watson Studio Visual Recognition will tell you what it thinks it found in your images! Beside each class of object (or color, age, etc.), it gives you a confidence score (between 0 and 1) showing how confident it is that it found that particular object or feature in your image (0 for lowest confidence and 1 for highest confidence).

<img src="images/29.png" width = "600">


6. Use the check boxes on the left to filter the images. In this example, only images in which Watson VR has detected **beige color** are displayed.

<img src="images/30.png" width = "600">


7. Use the **Threshold** slider to only display images in which Watson VR has at least 90% confidence of the beige color.

<img src="images/31.png" width = "600">


#### Task 3: Save a screenshot

**Note:** The screenshot saved in this step will be required as part of the Graded Final Assignment for those pursuing a certificate for this course. This step is optional for those auditing the course.


1. From Task 2, choose just one of the images that you uploaded. Select an image that does not have too many classes of objects. 

2. Take and save a screenshot in .jpeg or .jpg format including the Watson Visual Recognition confidence scores (that are indicated below the image). Ensure the labels and confidence scores below the picture are readable. See the  sample screenshot below.

<img src="images/32.png" width = "600">


#### Task 4: Share your results

Follow us on Twitter and send us some of the funniest and most interesting results you found with IBM Watson Visual Recognition!

<img src="images/33.png" width = "600">


<a href=https://twitter.com/intent/tweet >Click here</a> to share the above Tweet.

<a href=https://twitter.com/ravahuja >Follow Rav Ahuja</a>















