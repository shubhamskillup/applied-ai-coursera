<img src="images/IDSNlogo.png" width = "300">

# Hints

Create a separate data collection in IBM Watson Discovery.

Use the html documents from Coursera Help Articles (https://learner.coursera.help/hc/en-us/categories/201216173-Help-Articles) below.

[Coursera_help_centre_articles.zip](https://d3c33hcgiwev3.cloudfront.net/MHRnuOFXEemnvhJSBz-0XA_b08a735e2fa14d728162b4fcc2925daf_Coursera_help_centre_articles.zip?Expires=1600387200&Signature=KTmEV-BR4Aw-033LwkvdtVaSDVD-wmo30VT3N1agfGWKi-jrNRvUSa7ejdjjOzFqOHClJbT1sfoUCEB2JM9bamdgzjdVm6X9vPIGTJjHewUf5jQ7En67a3m-W2rAlF8c76yY81cAeW3nK2zV4kyddeWI6a0Y~4~4htLLJ9bXgIk_&Key-Pair-Id=APKAJLTNE6QMUY6HBC5A)

Similar to Lab 4, add enrichments to your data (Keyword Extraction).

<img src="images/46.png" width = "600">

After the enrichment process, your collection would look like the image below. Please make a note of its collection ID since each collection has a unique id associated to it and we want to differentiate it from Coursera Course Collection we created before.

<img src="images/47.png" width = "600">

Now, consider creating intents in your Watson Assistant for answering commonly asked questions.

Then, create a general node for the intents you created earlier. We only created a few intents as an example.



We would pass in intent and collectionId of the Coursera Help Articles as a parameter. Here's an example.

<img src="images/48.png" width = "600">

Make sure your return variable is set to webhook_result_2.

<img src="images/49.png" width = "600">

In the child node of Help Centre node, put the below lines as a response .

 ```
<? $webhook_result_2.response.result.res ?> 
<br>
Learn more at: <a href='https://learner.coursera.help/hc/en-us/categories/201216173' target="_blank"> https://learner.coursera.help/hc/en-us/categories/201216173</a>
 ```
 This is how you handle connecting to two different collections and making your chatbot more useful in the process.

 ## Review criteria

Your assignment will be graded by your peers who already submitted their own work.

The **main grading criteria** are:

- Did you provide a link to a preview of your chatbot?
- Does your chatbot improve upon the basic chatbot we developed within the course?
- Does your chatbot provide answers or links to relevant resources on the Coursera Help Center (https://learner.coursera.help/) when the user asks it common customer service questions?
- Does your chatbot fail to reply at all to the user at any time? (The goal is to always have an answer for the user, even if it's a simple, "I don't understand. Could you try rephrasing it?".)
- Does the chatbot responds with an adequate answer to the question, "What can you do?".
- Does it handle typical chit-chat interactions such as greetings, thank you, et cetera?
- Would your chatbot help a fellow Coursera student with their common questions?

You **will not be judged** on:

- Your mastery of the English language. The assignment requires an English submission so that international peers can review it, but we fully understand that learners have varying degrees of proficiency in English. Graders will not focus on this factor.
- Deployment or voice services. For this assignment, we don't need you to deploy the chatbot or make it work with Speech APIs. The Preview integration link is good enough to share the chatbot with your peer reviewer.

**If you are the grader, please be fair. Do not be too strict.**