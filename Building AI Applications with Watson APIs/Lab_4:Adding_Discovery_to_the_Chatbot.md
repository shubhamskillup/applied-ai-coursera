<img src="images/IDSNlogo.png" width = "300">

# Lab 4: Adding Discovery to the Chatbot

In the previous lab, we defined a basic chatbot that can offer hard-coded responses to some chitchat interactions and intents. However, Coursera's catalog is too big for us to provide specific course recommendations directly in the response section of a node.

Instead, in this lab, you will integrate the Watson Discovery collection we previously defined with the chatbot. We'll do so by using IBM Cloud Functions to make programmatic calls from said dialog node.

Let's briefly learn about IBM Functions. Functions offer serverless computing that runs code in response to events or direct invocations. Think of it as a code routine that can run in the cloud whenever you need it to. In other words, we can define an action, a small piece of code that can be invoked or set to automatically run in response to an event.

At the end of this lab, your chatbot will be able to call a cloud Function to receive a response from Watson Discovery and present course recommendations to the user.

#### How to structure a basic query

Since JSON is hierarchical, queries needs to be written using the same structured format. So, if the JSON document within a Discovery collection looks like this:

 ```
 "enriched_description": {
  "concepts": [
    {
    "text": "Cloud computing",
    "relevance": 0.610029}
    ]
  }


 ```

Your query would be structured like this: **enriched_description.concepts.text:Cloud computing**. The concept tagging enrichment understands how concepts relate and identifies general concepts that are not directly referenced in the description field. This particular query will return documents where Cloud computing was identified as a concept.

### Write a serverless action

Let's write an action that connects to Discovery API, runs the query with the user input, and then returns the result of the query to the dialog node.

1. From your Dashboard, **click on the Discovery service you created in Lab 1**. (Click on the name, not the icon next to it.)


<img src="images/14.png" width = "600">

2. **Click on the copy icon** to make note of the **API Key** and then **URL**. This **API Key** is the **password** for **serverless.yaml**. **URL** is location specific.

3. Open the **Coursera course catalog collection** by clicking the **Launch Watson Discovery** button. The collection overview information will be presented as shown in the image below.



<img src="images/15.png" width = "600">

4. **Click on api in the top right corner** and make note of **Collection ID** and **Environment ID**. We are going to need these to query the collection from our Function.

****If you do not see enrichments added to your data (Concept Tagging, Keyword Extraction), please go back to the previous lab and add enrichment correctly since the later step will use enriched documents. Hint: Upload the 500 documents again.****



<img src="images/16.png" width = "600">

5. Go to https://labs.cognitiveclass.ai/ and sign up. We are going to use the lab environment provided by SN Labs.

<img src="images/17.png" width = "600">

6. Click **JupyterLab**.

<img src="images/18.png" width = "600">

7. Once the Jupyter Lab is opened, click **Python 3** Under Notebook.

<img src="images/19.png" width = "600">

**Quick Jupyter Lab Tutorial**

While a cell is running, an asterisk (*) will appear in the square brackets to the left of the cell, [*]. Once the cell is finished, a number will replace the asterisk and will indicate the order in which the cells have been run. Please make sure the cell is finished before moving onto the next step.

The screenshot at left below is an example of a running cell and at right below is an example of finished cell.

<img src="images/20.png" width = "600">



8. In the cell, please copy the following exactly as below. Then **Press the Play button highlighted in the screenshot below in Blue** to run the cell. This will copy the scripts and configuration we'll need, in your lab.

 ```
%%bash
mkdir lab4
cd lab4
git clone https://github.com/sophiajwchoi/Lab4-Adding-Discovery-to-Chatbot.git
 ```

<img src="images/21.png" width = "600">

9. At this point, you should see a folder (lab4) listed in the files directory in the left-sidebar of the JupyterLab environment. If this side menu is hidden, you can go to View>View Left-Sidebar. Select the **lab4** and click on the  **Lab4-Adding-Discovery-to-Chatbot** subfolder. You will see a file called **serverless.yml**.

Replace the value of **YOUR_USERNAME** to **"apikey"**, **YOUR_DISCOVERY_PASSWORD** to the **API key** from your Discovery service. Similarly, replace the values of **ENVIRONMENT_ID** and **COLLECTION_ID** with the values obtained earlier above. Replace the value of **YOUR_DISCOVERY_URL** with the **url** we obtained earlier. **URL** is location specific.

Make sure to **press control + S** (Windows) or **command + S** (MacOS) to save the configuration.

<img src="images/22.png" width = "600">

10. Then, **create another cell by clicking at the + button on the top highlighted in blue below**. In the cell you just created, **copy the following lines exactly**. Press **Play button** to run the cell. Run the cell **only once**.

 ```
 %%bash
cd /resources/lab4/Lab4-Adding-Discovery-to-Chatbot
export npm_config_loglevel=silent
conda config --set notify_outdated_conda false
conda install nodejs -y
rm -f ~/.npmrc
npm install

  ```
<img src="images/23.png" width = "600">

11. **Optional Step:** Open the **CourseAdvisor.js** file and spend some time investigating the code. In particular, have a look at line 97:

The description field of our collection was enriched with Watson insights and we can query on **concepts** and **keyword**. Since the **keyword extraction** enrichment identifies content typically used when indexing data, generating tag clouds, or searching, we will run a query on **keyword** to recommend a course based on the user input.

12. Next you will **login to your IBM Cloud account** with your IBM Cloud email and password. **Please ensure that you are logged in before running lines starting with ibmcloud**. To login to your account, Create a new cell (as detailed earlier) and **copy the below lines exactly in the cell you created**. Replace YOUR_IBMCLOUD_EMAIL with your **IBM Cloud email** and **YOUR_IBMCLOUD_PASSWORD** with your password. Finally, **run the cell**, by pressing the play button (as mentioned earlier above). It may take a few minutes to finish. Please run the cell once. Please proceed to **step 13** if you have successfully logged in without an error message. See the screenshot below as an example.

 ```
 %%bash
ibmcloud config --check-version=false
ibmcloud login --no-region
YOUR_IBMCLOUD_EMAIL
YOUR_IBMCLOUD_PASSWORD

  ```

<img src="images/24.png" width = "600">

If you get the following **error** message, **"FAILED Unable to authenticate. You are using a federated user ID"**, you can either 1) create an IBM Cloud account with your personal email (not an ibm.com) and try again or 2) Create an API key using your federated IBM Cloud account(https://cloud.ibm.com/docs/iam?topic=iam-userapikey#create_user_key)  and **copy the following lines** below and replace **YOUR_APIKEY** with your actual **API Key**.

 ```
%%bash
ibmcloud login --apikey YOUR_APIKEY
  ```

  13. Copy the lines below exactly in the next cell. We will use the name of org under Name and Region in the next cell.

 ```
%%bash
ibmcloud account orgs

 ```

<img src="images/25.png" width = "600">

14. Copy the lines below exactly in the new cell and replace **REGION** with **region** and **Name_OF_ORG** with the **name of org **you obtained above. If you are re-doing this step, please remove **ibmcloud account space-create 'lab4'** because you already created the space and it would give an error.

 ```
%%bash
ibmcloud target --cf-api 'https://api.REGION.cf.cloud.ibm.com' -r REGION -o NAME_OF_ORG
ibmcloud account space-create 'lab4'

 ```

<img src="images/26.png" width = "600">

15. Copy the lines below and **replace REGION (in line numbers 7 and 8) with region you obtained above in the new cell** and **run the cell**. Finally, run the cell, by pressing the play button (as mentioned earlier above). It may take a few minutes to finish. Please run the cell once. If you get **npm not found** error, please **re-run step 10** and then **re-run step 15**.

 ```
%%bash
cd /resources/lab4/Lab4-Adding-Discovery-to-Chatbot
ibmcloud target -s lab4
ibmcloud plugin install cloud-functions -f
export npm_config_loglevel=silent
npm install -g serverless@1.51.0
ibmcloud fn --apihost REGION.functions.cloud.ibm.com
ibmcloud fn list --apihost REGION.functions.cloud.ibm.com
serverless deploy

 ```

 16. Next **create a new cell** and **copy the line below exactly** in the new cell you created. **Run the cell** you created to get the name of the **action**. You should see your action under **actions**. **Note the name of your action which starts from the / and ends with connectDiscovery.**

  ```
!ibmcloud fn list
  ```
Expected outcome is:

<img src="images/27.png" width = "600">

17. Log in to IBM Cloud and then search for **Functions** in the search bar. Select Functions. After the page is loaded, **make sure the top right hand side shows the name of your organization_lab4**. If your organization is abcd@gmail.com, then you should see the following, abcd@gmail.com_lab4. Click **Actions**.

<img src="images/28.png" width = "600">

18. Click connectDiscovery action under lab4 package.

<img src="images/29.png" width = "600">

19. Click **Endpoints** on the left sidebar.

<img src="images/30.png" width = "600">

20. Put a check mark beside **Enable as Web Action**. Click **copy Icon** to copy the URL for the action and make a copy of the REST API URL. Next, click **API-KEY** under REST API.

<img src="images/31.png" width = "600">

21. **Click on the copy icon** to obtain your **CF-based API key** and make a copy of the credentials.

<img src="images/32.png" width = "600">

You're now all set to begin integrating this function with Watson Assistant.

## Integrate Discovery with our Chatbot

1. From your Dashboard, click on the **Watson Assistant service** you created in Lab 3. (Click on the name, not the icon next to it.) and then click **Launch Watson Assistant**.


<img src="images/33.png" width = "600">

2. Under **Skills** tab, click **Student Advisor** skill. Then, click the **Options** tab.

<img src="images/34.png" width = "600">

3. Click **Webhooks**. In the **URL** field, specify the **REST API URL** for the action. Append a **?blocking=true** parameter to the action URL to force a synchronous call to be made (e.g. https://eu-gb.functions.cloud.ibm.com/api/v1/namespaces/YOUR-ORG_YOUR-SPACE/actions/lab4/connectDiscovery?blocking=true). Finally, click **Add authorization**.

<img src="images/35.png" width = "600">

4. We'll use the **CF-based API key** you obtained from action endpoints. The part before the **colon(:)** is your **User name**. Copy this to the User name field. Similarly, the part after the colon(:) is your **password**. (e.g. Your action API key = User name:password). Finally **save**.

<img src="images/36.png" width = "600">

5. Find **Courses** node under the **Dialog** tab. Click the **3 dots icon** in the Courses node and click **Add child node**. Then, name the new node (e.g., Discovery).

<img src="images/37.png" width = "600">

6. Click the **Courses node** and then click **Customize** to customize the node.

<img src="images/38.png" width = "600">

7. Scroll down to the **_Webhooks_** section, and switch the toggle to **On**, and then click **Apply**.

<img src="images/39.png" width = "600">

8. Now, we will add a parameter, input as a key and "<?input.text?>" as a value.

<img src="images/40.png" width = "600">

9. Scroll down and set the Return variable to **webhook_result_1**.

<img src="images/41.png" width = "600">

10. Scroll down and under And **finally**, select **Jump to option** and select the child node you created and then select If **assistant recognizes**(condition).

<img src="images/42.png" width = "600">

<img src="images/43.png" width = "600">

11. Put **true** as a condition for the node, as we always want to execute it when giving course recommendations. Add the following sample response, which retrieves a list of relevant courses from Discovery via the Function we defined earlier on.

 ```
Here are some courses I found for you! </br> 
<a href='<? $webhook_result_1.response.result.courses.get(0).link ?>' target="_blank"><? $webhook_result_1.response.result.courses.get(0).name ?></a> <? $webhook_result_1.response.result.courses.get(0).description ?> </br> <a href='<? $webhook_result_1.response.result.courses.get(1).link ?>' target="_blank"><? $webhook_result_1.response.result.courses.get(1).name ?></a> <? $webhook_result_1.response.result.courses.get(1).description ?>  </br> <a href='<? $webhook_result_1.response.result.courses.get(2).link ?>' target="_blank"><? $webhook_result_1.response.result.courses.get(2).name ?></a> <? $webhook_result_1.response.result.courses.get(2).description ?> 

 ```

<img src="images/44.png" width = "600">

12. Now you're all set. Try running **Recommend me a course on databases** in **Try it out panel** to confirm that the node works as expected. If you see a result similar to the image below, you are all set for this lab.

To recap, we created a Cloud Function that connects to our Discovery collection and retrieves documents relevant to the user query. We then invoke that function from the relevant node, when people express the intent of receiving a course recommendation. The result is then displayed to the user in our response.



<img src="images/45.png" width = "600">

In short, we managed to dynamically invoke results rather than hardcoding the responses. This is a very powerful and flexible approach. You'll use it again when working on the capstone project.







